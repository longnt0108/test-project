git clone this project

cd into the project folder

update DATABASE_URL in .env or in your .env.local

composer install

php bin/console doctrine:database:create

php bin/console doctrine:migrations:migrate

php bin/console doctrine:fixtures:load

symfony server:start

Then you can login using

username: admin
password: admin
